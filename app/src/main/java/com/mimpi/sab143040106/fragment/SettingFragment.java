package com.mimpi.sab143040106.fragment;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mimpi.sab143040106.ProfileSaya;
import com.mimpi.sab143040106.R;

public class SettingFragment extends Fragment {

    public static ProfileSaya profileSaya;

    public static SettingFragment newInstance(ProfileSaya activity){
        profileSaya = activity;
        return new SettingFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //return inflater.inflate(R.layout.fragment_tab1, container, false);
        View v = inflater.inflate(R.layout.activity_setting_fragment, container, false);


        return v;
    }
}
