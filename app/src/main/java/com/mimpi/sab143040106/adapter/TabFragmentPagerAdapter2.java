package com.mimpi.sab143040106.adapter;

/**
 * Created by mimpi on 3/5/2017.
 */

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.mimpi.sab143040106.fragment.ContactFragment;
import com.mimpi.sab143040106.fragment.GalleryFragment;
import com.mimpi.sab143040106.fragment.ProfileFragment;
import com.mimpi.sab143040106.fragment.SettingFragment;

public class TabFragmentPagerAdapter2 extends FragmentPagerAdapter {
    //nama tab nya
    String[] title = new String[]{
            "PROFIL", "GALLERY", "CONTACT", "SETTING"
    };

    public TabFragmentPagerAdapter2(FragmentManager fm) {
        super(fm);
    }

    //method ini yang akan memanipulasi penampilan Fragment dilayar
    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        switch (position){
            case 0:
                fragment = new ProfileFragment();
                break;
            case 1:
                fragment = new GalleryFragment();
                break;
            case 2:
                fragment = new ContactFragment();
                break;
            case 3:
                fragment = new SettingFragment();
                break;
            default:
                fragment = null;
                break;
        }

        return fragment;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return title[position];
    }

    @Override
    public int getCount() {
        return title.length;
    }
}